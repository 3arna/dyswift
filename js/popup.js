window.addEvent('domready', function() {
    $$(".openpopup").addEvent('click', function(){
        var popUp = new popup({});
        var title = this.get('title');
        popUp.load(title);
    });
});




var popup = new Class({

    Implaments: [Options],

    options:{
        fogid : "fog",
        domWidth : "960",
        domHeight : "500",
        popupid : "popup",
        popupWidth : "400",
        popupHeight : "400"
    },

    initialize : function(){
        
    },
    
    load : function(page){
        this.fogSize();
        this.setpopupPosition();

        $(this.options.fogid).setStyles({
            display : 'block',
            height : this.options.domHeight
        });

        $(this.options.popupid).setStyle('display', 'block');

        $('popup-title').set('text',page);

        $('popup-text-container').load('http://localhost/dyswift/popups/'+page+'.html');
    },

    fogSize : function(){
        var scrollSize = $(document.body).getScrollSize();
        this.options.domHeight = scrollSize.y;
        this.options.domWidth = scrollSize.x;
    },

    setpopupPosition : function(){
        $(this.options.popupid).setPosition({
            x:(this.options.domWidth/2)-(this.options.popupWidth/2),
            y:((this.options.domHeight/2)-(this.options.popupHeight/2))
        });
    },
    
    close : function(){
        $(this.options.fogid).setStyle('display', 'none');
        $(this.options.popupid).setStyle('display', 'none');
    }

});