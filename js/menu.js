window.addEvent('domready', function() {

    $$('.contents').setStyle('display', 'none');
    $('Groups').setStyle('display', 'block');

    $$("#menu li").addEvent('click', function(){
        var text = this.get('text');
        $$("#menu li").removeClass('menu-active');
        this.addClass('menu-active');
        $$(".contents").setStyle('display', 'none');
        $(text).setStyle('display', 'block');
        
    });
});